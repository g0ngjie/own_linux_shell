#!/bin/bash

exec_docker_mongo(){
    sudo docker exec -it mongodbserver bash
}

exec_docker_mongo_root(){
    sudo docker exec -u root -it mongodbserver bash
}

exec_docker_stop(){
    cd $HOME/workspace/rits_documanage
	sudo docker-compose stop
}

mongo_store(){
	case $1 in
	'root')
	echo root
	exec_docker_mongo_root
	;;
	*)
	exec_docker_mongo
	;;
	esac
}

exec_docker_show_ip(){
    sudo docker inspect -f '{{.Name}}-{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(sudo docker ps -q)|awk '{split($1,args,"-");print args[1],args[2]}'
}

show_store(){
    case $1 in
	*)
	exec_docker_show_ip
	;;
    esac
}

dispatch(){
    case $1 in
	'mongo')
	mongo_store $2
    ;;
	'show')
	show_store $2
	;;
	'stop')
	exec_docker_stop
    ;;
	*)
	echo invalid operation
	;;
    esac
}

dispatch $1 $2
