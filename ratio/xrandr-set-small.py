import subprocess

def run_script(script):
    res = subprocess.Popen(script, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) # 使用管道
    return res.stdout.read()  # 标准输出


    
if __name__ == "__main__":
    
    result = run_script('sudo cvt 1920 980').decode()
    split_str = '"1920x980_60.00"'
    result_str = split_str + result.split(split_str, 1)[1]
    set_result = run_script('sudo xrandr --newmode ' + result_str)
    run_script("sudo xrandr --addmode Virtual1 " + split_str)
    run_script("sudo xrandr --output Virtual1 --mode " + split_str)
