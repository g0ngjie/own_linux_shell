import subprocess

def run_script(script):
    res = subprocess.Popen(script, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) # 使用管道
    return res.stdout.read()  # 标准输出


    
if __name__ == "__main__":
    
    result = run_script('sudo cvt 1920 1080').decode()
    split_str = '"1920x1080_60.00"'
    result_str = split_str + result.split(split_str, 1)[1]
    # print(result_str)

    set_result = run_script('sudo xrandr --newmode ' + result_str)
    # show_q = run_script("sudo xrandr -q| awk '{print $1}'|grep Virtual")
    run_script("sudo xrandr --addmode Virtual1 " + split_str)
    run_script("sudo xrandr --output Virtual1 --mode " + split_str)


    # print(type(show_q))
    # print(range(show_q))
    # for value in range(show_q):
    #     print(value)
    # # print(show_q)
    
