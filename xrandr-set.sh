#!/bin/bash

nomal(){
    python3 xrandr-set.py
}

small(){
    python3 xrandr-set-small.py
}

main(){
    cd /home/gongjie/vimwork/ratio
    if [ "$1" = 'small' ];then
        small
    else
        nomal
    fi
}

main $1